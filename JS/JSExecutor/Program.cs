﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace JSExecutor
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver webDriver = new ChromeDriver();
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)webDriver;
            webDriver.Manage().Window.Maximize();
            webDriver.Navigate().GoToUrl("https://www.google.com/");
            IWebElement searchBar = webDriver.FindElement(By.XPath("//input[@class='gLFyf gsfi']"));
            Click(jsExecutor, searchBar);
            SendKeys(jsExecutor, searchBar, "WebDriver");
            searchBar.Submit();
            ScrollToEnd(jsExecutor);
            ScrollToBegin(jsExecutor);
            webDriver.Quit();
        }
        static void Click(IJavaScriptExecutor jsExecutor, IWebElement webElement)
        {
            jsExecutor.ExecuteScript("arguments[0].click();", webElement);
        }
        static void SendKeys(IJavaScriptExecutor jsExecutor, IWebElement webElement, string message)
        {
            jsExecutor.ExecuteScript($"arguments[0].value = '{message}';", webElement);
        }
        static void ScrollToEnd(IJavaScriptExecutor jsExecutor)
        {
            jsExecutor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);");
        }
        static void ScrollToBegin(IJavaScriptExecutor jsExecutor)
        {
            jsExecutor.ExecuteScript("window.scrollTo(0, 0);");
        }
    }
}
