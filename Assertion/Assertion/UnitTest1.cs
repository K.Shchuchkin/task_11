using NUnit.Framework;

namespace Assertion
{
    public class UnitTest1
    {
        [Test]
        public void AssertionTests()
        {
            A a = new A();
            A b = a;
            A c = new A();
            Assert.Multiple(() =>
                {
                    Assert.AreEqual(a, b);
                    Assert.AreSame(a, b, "Not same");
                    Assert.True(a == b, "Not true");
                    Assert.GreaterOrEqual(42, 21, "Less");
                }
            );
        }
    }
    public class A
    {

    }
}
